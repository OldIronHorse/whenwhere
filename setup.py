from setuptools import setup

setup(
  name='whenwhere',
  version='0.1',
  description='Time tabling testbed.',
  url='https://github.com/oldironhorse/whenwhere',
  author='Simon Redding',
  author_email='s1m0n.r3dd1ng@gmail.com',
  license='GPL 3.0',
  packages=['whenwhere'],
  install_requires=[
  ],
  test_suite='nose.collector',
  tests_require=['nose'],
  zip_safe=False)

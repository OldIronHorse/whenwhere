from unittest import TestCase
from datetime import datetime

from whenwhere import events_by_date, events_by_owner, Event


class TestEventsByOwner(TestCase):
    def test_empty_calendar(self):
        self.assertEqual([], list(events_by_owner([], "Bill")))

    def test_no_matches(self):
        self.assertEqual([],
                         list(events_by_owner([Event(None, 'Bob', None)],
                                              "Bill")))

    def test_one_match_lone(self):
        self.assertEqual([Event(None, 'Bob', None)],
                         list(events_by_owner([Event(None, 'Bob', None)],
                                              "Bob")))

    def test_one_match(self):
        self.assertEqual([Event(None, 'Bob', None)],
                         list(events_by_owner([Event(None, 'Bob', None),
                                               Event(None, 'Bill', None)],
                                              "Bob")))

    def test_multiple_match(self):
        self.assertEqual([Event(None, 'Bob', None),
                          Event(None, 'Bob', None)],
                         list(events_by_owner([Event(None, 'Bob', None),
                                               Event(None, 'Bill', None),
                                               Event(None, 'Bob', None)],
                                              "Bob")))


class TestEventsByDate(TestCase):
    def setUp(self):
        self.calendar = [
            Event('e1', None, datetime(2020, 8, 3)),
            Event('e2', None, datetime(2020, 9, 17)),
            Event('e3', None, datetime(2020, 9, 16)),
            Event('e4', None, datetime(2020, 2, 1))
        ]

    def test_empty_calendar(self):
        self.assertEqual([],
                         list(events_by_date([], datetime(2020, 9, 17))))

    def test_range_empty_calendar(self):
        self.assertEqual([],
                         list(events_by_date([],
                                             datetime(2020, 9, 17),
                                             datetime(2020, 10, 2))))

    def test_single_date(self):
        self.assertEqual([Event('e2', None, datetime(2020, 9, 17))],
                         list(events_by_date(self.calendar,
                                             datetime(2020, 9, 17))))

    def test_date_range(self):
        self.assertEqual([Event('e2', None, datetime(2020, 9, 17)),
                          Event('e3', None, datetime(2020, 9, 16))],
                         list(events_by_date(self.calendar,
                                             datetime(2020, 9, 1),
                                             datetime(2020, 10, 1))))

    def test_date_range_exact(self):
        self.assertEqual([Event('e2', None, datetime(2020, 9, 17)),
                          Event('e3', None, datetime(2020, 9, 16))],
                         list(events_by_date(self.calendar,
                                             datetime(2020, 9, 16),
                                             datetime(2020, 9, 17))))

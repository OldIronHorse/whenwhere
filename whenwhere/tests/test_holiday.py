from unittest import TestCase
from datetime import datetime

from whenwhere import Event, whos_on_holiday, how_many_days_taken, Person, \
    take_holiday, InsufficientHolidayRemainingException


class TestHolidayCalendar(TestCase):
    def setUp(self):
        self.calendar = [
            Event(None, "Simon", datetime(2018, 6, 1)),
            Event(None, "Pete", datetime(2018, 6, 1)),
            Event(None, "Tae", datetime(2018, 6, 1)),
            Event(None, "Tatiana", datetime(2018, 6, 1)),
            Event(None, "Tatiana", datetime(2018, 6, 4)),
            Event(None, "Tatiana", datetime(2018, 6, 7)),
            Event(None, "Chris", datetime(2018, 6, 7)),
            Event(None, "Chris", datetime(2018, 6, 8)),
            Event(None, "David", datetime(2018, 6, 8)),
            Event(None, "Chris", datetime(2018, 6, 11)),
            Event(None, "Daniel", datetime(2018, 6, 14)),
            Event(None, "Daniel", datetime(2018, 6, 15)),
            Event(None, "Lucia", datetime(2018, 6, 15)),
            Event(None, "David", datetime(2018, 6, 28)),
            Event(None, "Filippo", datetime(2018, 6, 28)),
            Event(None, "Lucia", datetime(2018, 6, 29)),
            Event(None, "David", datetime(2018, 6, 29)),
            Event(None, "Henry", datetime(2018, 6, 29)),
            Event(None, "Filippo", datetime(2018, 6, 29)),
            Event(None, "Janani", datetime(2018, 6, 29))
        ]

    def test_whos_on_holiday_day_none(self):
        self.assertEqual([],
                         list(whos_on_holiday(self.calendar,
                                              datetime(2018, 6, 5))))

    def test_whos_on_holiday_day_single(self):
        self.assertEqual(['Tatiana'],
                         list(whos_on_holiday(self.calendar,
                                              datetime(2018, 6, 4))))

    def test_whos_on_holiday_day_multiple(self):
        self.assertEqual(['Simon', 'Pete', 'Tae', 'Tatiana'],
                         list(whos_on_holiday(self.calendar,
                                              datetime(2018, 6, 1))))

    def test_how_many_days_taken_none(self):
        self.assertEqual(0, how_many_days_taken(self.calendar,
                                                'Zebedee',
                                                datetime(2018, 6, 1),
                                                datetime(2018, 6, 30)))

    def test_how_many_days_taken(self):
        self.assertEqual(2, how_many_days_taken(self.calendar,
                                                'Filippo',
                                                datetime(2018, 6, 1),
                                                datetime(2018, 6, 30)))
        self.assertEqual(3, how_many_days_taken(self.calendar,
                                                'Tatiana',
                                                datetime(2018, 6, 1),
                                                datetime(2018, 6, 30)))


class TestTakeHoliday(TestCase):
    def test_single_day_sufficent_entitlement(self):
        self.assertEqual((Person('Bob', 25, 6),
                          [Event(None, 'Bob', datetime(2019, 8, 3))]),
                         take_holiday(Person('Bob', 25, 7),
                                      [],
                                      datetime(2019, 8, 3)))

    def test_single_day_insufficent_entitlement(self):
        with self.assertRaises(InsufficientHolidayRemainingException):
            take_holiday(Person('Bob', 25, 0),
                         [],
                         datetime(2019, 8, 3))

    def test_multiple_days_sufficent_entitlement(self):
        self.assertEqual((Person('Bob', 25, 6),
                          [Event(None, 'Bob', datetime(2019, 1, 7)),
                           Event(None, 'Bob', datetime(2019, 1, 8)),
                           Event(None, 'Bob', datetime(2019, 1, 9))]),
                         take_holiday(Person('Bob', 25, 9),
                                      [],
                                      datetime(2019, 1, 7),
                                      3))

    def test_multiple_days_insufficent_entitlement(self):
        with self.assertRaises(InsufficientHolidayRemainingException):
            take_holiday(Person('Bob', 25, 3),
                         [],
                         datetime(2019, 8, 3),
                         5)

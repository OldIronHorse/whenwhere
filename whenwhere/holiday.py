from .whenwhere import events_by_date, events_by_owner, Event
from collections import namedtuple
from datetime import timedelta

Person = namedtuple('Person', 'name, entitlement remaining')


class InsufficientHolidayRemainingException(Exception):
    pass


def whos_on_holiday(calendar, date):
    return (e.owner for e in events_by_date(calendar, date))


def how_many_days_taken(calendar, person, start, end):
    return sum(1 for e in
               events_by_date(events_by_owner(calendar, person), start, end))


def take_holiday(person, calendar, start_date, duration=1):
    if person.remaining - duration < 0:
        raise InsufficientHolidayRemainingException()
    days = [Event(None, person.name, start_date + timedelta(days=n))
            for n in range(0, duration)]
    return (person._replace(remaining=person.remaining - len(days)),
            calendar + days)

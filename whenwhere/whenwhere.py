from collections import namedtuple

Event = namedtuple('Event', 'title owner date')


def events_by_owner(calendar, person):
    return (e for e in calendar if e.owner == person)


def events_by_date(calendar, start, end=None):
    if end is None:
        end = start
    return filter(lambda e: e.date >= start and e.date <= end, calendar)
